from Helper.__comp import *
from random import choice
import json, time
from Helper.__config import STARTUP
from Helper.__functions import m_line, command_user, is_dm
from Helper.__server_functions import member_check

def setup(BOT):
	BOT.add_cog(Remindme(BOT))

class Remindme(cmd.Cog):
	'''
	Schedules a reminder to be sent to you in DMS after the specified duration. Format the duration as `00d00h00m` but replace `00` with the actual number of days, hours, and minutes. If you have zero of those units, you can ignore them (like `10m`).
	'''

	# Extra arguments to be passed to the command
	FORMAT = "(duration) (content)"
	CATEGORY = "UTILITY"
	EMOJI = CATEGORIES[CATEGORY]
	ALIASES = []

	def __init__(self, BRAIN):
		self.BRAIN = BRAIN
		return

	@cmd.command(name="remindme")
	@cmd.cooldown(1, 3, cmd.BucketType.user)
	@cmd.check(member_check)
	async def reminder_me(self, ctx, duration, *, content="you forgot to actually provide reminder content lmao"):
		timenow = time()
		with open('DB/reminders.json') as json_file:
			reminders = json.load(json_file)

		if len([x for x in reminders if x['owner'] == command_user(ctx).id]) >= 40: 
			await ctx.respond("You cannot have more than 40 pending reminders at once!", ephemeral=True)
		
		times = {"d":0,"h":0,"m":0}
		currStr = ""
		for x in str(duration):
			if x not in "1234567890":
				try: 
					times[x]
					times[x] = int(currStr)
				except: raise cmd.errors.ArgumentParsingError()
			else: currStr += x

		channel = command_user(ctx).dm_channel
		if channel is None: channel = await command_user(ctx).create_dm()

		addedTime = times['d'] * 86400 + times['h'] * 3600 + times['m'] * 60

		addReminder = {'time':addedTime + timenow - timenow%60 - 1, 'channel': str(channel.id), 'content':content, 'owner':command_user(ctx).id, 'type':f'personal {int(timenow)}'}

		reminders += [addReminder]
		open("DB/reminders.json","w").write(json.dumps(reminders,indent="\t"))

		await ctx.respond(f"Alright, reminding you in {duration}.", ephemeral=True)

		return
